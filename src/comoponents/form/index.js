import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

export const Form = () => {
  const schema = yup.object().shape({
    texto: yup
      .string()
      .required("obrigatório")
      .max(18, "maximo de 18 caracteres"),
    email: yup.string().email("email invalido").required("obrigatório"),
    senha: yup
      .string()
      .min(5, "minimo de 5 caracteres")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "senha fraca"
      )
      .required("obrigatório"),
    confirmarSenha: yup
      .string()
      .oneOf([yup.ref("senha")], "senhas diferentes")
      .required(),
    cpf: yup
      .string()
      .required()
      .matches(/^[0-9]+$/, "Must be only digits")
      .min(11, "Must be exactly 11 digits")
      .max(11, "Must be exactly 11 digits"),
    rg: yup
      .string()
      .required()
      .matches(/^[0-9]+$/, "Must be only digits"),
    game: yup.string().max(20, "maximo de 20 caracteres"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    console.log(data);
    reset();
  };

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <div>
        <input placeholder="texto" name="texto" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.texto?.message}
        </p>
      </div>
      <div>
        <input placeholder="email" name="email" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.email?.message}
        </p>
      </div>
      <div>
        <input placeholder="senha" name="senha" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.senha?.message}
        </p>
      </div>
      <div>
        <input
          placeholder="confirmar senha"
          name="confirmarSenha"
          ref={register}
        ></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.confirmarSenha?.message}
        </p>
      </div>
      <div>
        <input placeholder="CPF" name="cpf" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>{errors.cpf?.message}</p>
      </div>
      <div>
        <input placeholder="RG" name="rg" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>{errors.rg?.message}</p>
      </div>
      <div>
        <input placeholder="jogo favorito" name="game" ref={register}></input>
        <p style={{ color: "#a22", fontSize: "12px" }}>
          {errors.game?.message}
        </p>
      </div>
      <div>
        <button type="submit">enviar</button>
      </div>
    </form>
  );
};
